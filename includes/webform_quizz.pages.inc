<?php

/**
 * @file
 * webform_quizz.pages.inc
 * Menu callbacks and functions for configuring webforms quizzes.
 */

/**
 * Main configuration form for editing a webforn's quizz settings.
 */
function webform_quizz_configure_form($form, $form_state, $node) {
  dpm($node, 'node');
  $form = array();

  $form['is_quizz'] = array(
    '#type' => 'checkbox',
    '#title' => t('This webform is a quizz.'),
    '#default_value' => 0, //@FIXME
  );

  $form['quizz_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Quizz settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="is_quizz"]' => array('checked' => TRUE),
      ),
    ),
  );
/*
  $form['quizz_data']['test'] = array(
    '#type' => 'textfield',
    '#title' => 'test',
  );
*/
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}